<?php
/**
 * Created by PhpStorm.
 * User: leifei
 * Date: 2015/9/20
 * Time: 11:39
 */
class TblUser extends CActiveRecord{

    public static  function model($className=__CLASS__){
        return parent::model($className);
    }
    public function tableName(){
     //   return 'tbl_user';
        return '{{user}}';
    }


    public function getOneList(){
        $id=1;
        $user = Yii::app()->db->createCommand()
            ->select('u.id, username, phone')
            ->from('tbl_user u')
            ->join('tbl_profile p', 'u.id=p.id')
            ->where('u.id=:id', array(':id'=>$id))
            ->queryRow();
        return $user;
    }

}